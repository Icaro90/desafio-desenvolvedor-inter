package com.inter.desafio.service;

import com.inter.desafio.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User> findById(Long id);
    void create(User user);
    void update(User user);
    void delete(Long id);
}
