package com.inter.desafio.service;

import com.inter.desafio.model.DigitoUnico;

import java.util.List;

public interface DigitoUnicoService {
    List<DigitoUnico> findAll();
    Integer getDigitoUnico(String n, int k);
    void create(DigitoUnico digitoUnico);
    List<DigitoUnico> findByUserId(long userId);
}
