package com.inter.desafio.service.impl;

import com.inter.desafio.model.DigitoUnico;
import com.inter.desafio.repository.DigitoUnicoRepository;
import com.inter.desafio.service.DigitoUnicoService;
import com.inter.desafio.utils.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DigitoUnicoServiceImpl implements DigitoUnicoService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    @Override
    public List<DigitoUnico> findAll() {
        try {
            return (List<DigitoUnico>) digitoUnicoRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de dígitos únicos = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(DigitoUnico digitoUnico) {
        try {
            if (digitoUnico != null) {
                digitoUnicoRepository.save(digitoUnico);
            } else {
                log.warn("Digito único não pode ser nulo");
                throw new ResponseException("Digito único não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch (ResponseException ex) {
            log.error("Erro ao tentar salvar digito único, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public List<DigitoUnico> findByUserId(long userId) {
        try{
            return digitoUnicoRepository.findByUserId(userId);
        }catch (ResponseException ex) {
            log.error("Erro ao obter digito único do usuario id = {}, {}", userId, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Integer getDigitoUnico(String n, int k) {
        try {
            return processaDigitoUnico(n, k);
        } catch (ResponseException ex) {
            log.error("Erro ao obter digito único, {}", ex.getStatus());
            throw ex;
        }
    }

    public Integer processaDigitoUnico(String n, int k) {
        try {
            String digito = concatenaDigitos(n, k);

            if (digito.length() > 1)
                digito = somaDigitos(digito);

            return Integer.parseInt(digito);
        } catch (ResponseException ex) {
            log.error("Erro ao processar digito único, {}", ex.getStatus());
            throw ex;
        }
    }

    private String concatenaDigitos(String n, int k) {
        try {
            String digito = "";

            for (int i = 0; i < k; i++) {
                digito += n;
            }

            return digito;
        } catch (ResponseException ex) {
            log.error("Erro ao concatenar digito {}", ex.getStatus());
            throw ex;
        }
    }

    private String somaDigitos(String digito) {
        try {
            int number = 0;

            for (int i = 0; i < digito.length(); i++)
                number += Integer.parseInt("" + digito.charAt(i));

            return "" + number;
        } catch (ResponseException ex) {
            log.error("Erro ao somar digitos, {}", ex.getStatus());
            throw ex;
        }
    }
}
