package com.inter.desafio.service.impl;

import com.inter.desafio.model.User;
import com.inter.desafio.repository.UserRepository;
import com.inter.desafio.service.UserService;
import com.inter.desafio.utils.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {
        try {
            return (List<User>) userRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de usuários = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return userRepository.findById(id);
        } catch(ResponseException ex) {
            log.error("Erro ao obter usuário pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(User user) {
        try {
            if (user != null) {
                if(user.getDigitoUnicoList() == null)
                    user.setDigitoUnicoList(new ArrayList<>());

                userRepository.save(user);
            } else {
                log.warn("Usuário não pode ser nulo");
                throw new ResponseException("Usuário não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar criar usuário, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void update(User user) {
        try {
            if (user != null) {
                userRepository.save(user);
            } else {
                log.warn("Usuário não pode ser nulo");
                throw new ResponseException("Usuário não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar atualizar usuário, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<User> user = userRepository.findById(id);
            user.ifPresent(u -> userRepository.delete(u));
        } catch(ResponseException ex) {
            log.error("Erro ao tentar deletar usuário pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

}
