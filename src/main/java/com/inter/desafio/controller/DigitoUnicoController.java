package com.inter.desafio.controller;

import com.inter.desafio.model.DigitoUnico;
import com.inter.desafio.model.User;
import com.inter.desafio.service.DigitoUnicoService;
import com.inter.desafio.service.UserService;
import com.inter.desafio.utils.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/digito-unico")
public class DigitoUnicoController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private DigitoUnicoService digitoUnicoService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/new/{userId}"}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody DigitoUnico digitoUnico, @PathVariable("userId") Long id) {
        try {
            digitoUnico.setR(digitoUnicoService.getDigitoUnico(digitoUnico.getN(), digitoUnico.getK()));

            if (id != 0) {
                Optional<User> user = userService.findById(id);

                if (user.isPresent()) {
                    user.get().addToList(digitoUnico);
                    digitoUnico.setUser(user.get());
                }
            }else {
                digitoUnico.setUser(null);
            }

            digitoUnicoService.create(digitoUnico);
            log.info("Novo dígito único para {} calculado com sucesso", digitoUnico.getK());
            return new ResponseEntity<>("Novo dígito único calculado com sucesso!", HttpStatus.CREATED);
        } catch (ResponseException ex) {
            log.error("Erro ao criar lista de dígitos únicos = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<DigitoUnico> findAll() {
        try {
            return digitoUnicoService.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de dígitos únicos = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/getByUser/{userId}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<DigitoUnico> getByUserId(@PathVariable("userId") Long id) {
        try {
            return digitoUnicoService.findByUserId(id);
        } catch (ResponseException ex) {
            log.error("Erro ao obter usuário pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }
}
