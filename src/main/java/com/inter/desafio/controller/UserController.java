package com.inter.desafio.controller;

import com.inter.desafio.model.User;
import com.inter.desafio.service.UserService;
import com.inter.desafio.utils.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/all"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getAll() {
        try {
            return userService.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de usuários = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/get/{id}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Optional<User> getById(@PathVariable("id") Long id) {
        try {
            return userService.findById(id);
        } catch (ResponseException ex) {
            log.error("Erro ao obter usuário pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody User user) {
        try {
            userService.create(user);
            log.info("Novo usuário {} criado com sucesso", user.getEmail());

            return new ResponseEntity<>("Usuário criado com sucesso!", HttpStatus.CREATED);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(String.format("Erro ao tentar criar usuário, %s", ex.getStatus()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = {"/edit/{id}"}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable("id") Long id) {
        try {
            user.setId(id);
            userService.update(user);
            log.info("Usuário {} atualizado.", user.getId());

            return new ResponseEntity<>("Usuário atualizado com sucesso!", HttpStatus.OK);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(String.format("Erro ao tentar atualizar o usuário com id = %s, %s", id, ex.getStatus()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = {"/delete/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        try {
            userService.delete(id);
            return String.format("Usuário %s deletado com sucesso!", id);
        } catch (ResponseException ex) {
            return String.format("Erro ao tentar excluir usuário com id = %s, %s", id, ex.getStatus());
        }
    }
}
