package com.inter.desafio.repository;

import com.inter.desafio.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}

