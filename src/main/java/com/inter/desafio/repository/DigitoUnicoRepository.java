package com.inter.desafio.repository;

import com.inter.desafio.model.DigitoUnico;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DigitoUnicoRepository extends CrudRepository<DigitoUnico, Long> {
    @Query("SELECT d FROM DigitoUnico d WHERE d.user.id = :userId")
    List<DigitoUnico> findByUserId(@Param("userId") long userId);
}

