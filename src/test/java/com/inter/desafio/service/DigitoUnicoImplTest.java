package com.inter.desafio.service;


import com.inter.desafio.model.DigitoUnico;
import com.inter.desafio.repository.DigitoUnicoRepository;
import com.inter.desafio.service.impl.DigitoUnicoServiceImpl;
import com.inter.desafio.utils.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
public class DigitoUnicoImplTest {
    @Mock
    DigitoUnicoRepository digitoUnicoRepository;

    @InjectMocks
    @Spy
    DigitoUnicoServiceImpl digitoUnicoService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll() {
        List<DigitoUnico> response = new ArrayList<>();
        Mockito.when(digitoUnicoRepository.findAll()).thenReturn(response);

        digitoUnicoService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(digitoUnicoRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        digitoUnicoService.findAll();
    }

    @Test
    public void createTest() {
        DigitoUnico digit = newDigitoUnico();

        Mockito.when(digitoUnicoRepository.save(digit)).thenReturn(digit);

        digitoUnicoService.create(digit);

        Mockito.verify(digitoUnicoService, Mockito.atMostOnce()).create(digit);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {
        digitoUnicoService.create(null);

        Mockito.verify(digitoUnicoService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void getUniqueDigit() {
        DigitoUnico digit = newDigitoUnico();
        digitoUnicoService.getDigitoUnico(digit.getN(), digit.getK());

        assert digitoUnicoService.getDigitoUnico(digit.getN(), digit.getK()) == 6;

        Mockito.verify(digitoUnicoService, Mockito.atLeastOnce()).getDigitoUnico(digit.getN(), digit.getK());
    }

    public DigitoUnico newDigitoUnico() {
        return new DigitoUnico((long) 1, "123", 4);
    }

}
